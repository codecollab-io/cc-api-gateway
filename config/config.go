package config

import "github.com/spf13/viper"

type Config struct {
	ApplicationURL string
	PublicKey      string
	PrivateKey     string
	Debug          bool
	// CookieDomain will be set as the domain for token cookies.
	// This is omitted if Debug is true
	CookieDomain string

	ServiceMap struct {
		Auth   Service
		API    Service
		Editor Service
		LTI    Service
	}

	NewRelic struct {
		AppName string
		License string
	}

	Mongo struct {
		ConnectionString string
		Database         string
		UserCollection   string
	}
}

type Service struct {
	// Path refers to the path that will be mapped to the Endpoint
	// it MUST be prefixed with a "/"
	// For example, Path set as "/auth' will map https://gateway.cclb.dev/auth/* to https://service.cclb.dev/*
	Path string
	// Endpoint refers to the service endpoint
	// It must not have a trailing "/"
	// For example, https://cc-auth.cclb.dev
	Endpoint string
}

func NewConfig() (*Config, error) {
	viper.SetConfigName("config")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("config")

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	c := new(Config)
	err = viper.Unmarshal(c)
	if err != nil {
		return nil, err
	}

	return c, nil
}
