package proxy

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"github.com/newrelic/go-agent/v3/newrelic"
	"gitlab.com/codecollab-io/cc-api-gateway/config"
	"gitlab.com/codecollab-io/cc-api-gateway/middleware"
	"go.uber.org/zap"
	"google.golang.org/api/idtoken"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type Proxy interface {
	// Handler is the method that is passed to chi.Mux
	Handler(w http.ResponseWriter, r *http.Request)
	// Target parses the original path and returns a URL to the service
	target(path string) (*url.URL, error)
	// Proxy returns a *httputil.ReverseProxy for the address
	proxy(ctx context.Context, address *url.URL) *httputil.ReverseProxy
}

type Default struct {
	debug   bool
	service config.Service
}

func NewDefault(debug bool, service config.Service) Proxy {
	return &Default{debug, service}
}

func (e *Default) Handler(w http.ResponseWriter, r *http.Request) {
	target, err := e.target(r.URL.Path)
	if err != nil {
		zap.S().Errorf("error while getting target path: %v", err)
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	tokenSource, err := idtoken.NewTokenSource(r.Context(), e.service.Endpoint+"/")
	if err != nil {
		zap.S().Errorf("error while getting token source: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	token, err := tokenSource.Token()
	if err != nil {
		zap.S().Errorf("error while getting token source: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	token.SetAuthHeader(r)

	e.proxy(r.Context(), target).ServeHTTP(w, r)
}

func (e *Default) target(path string) (*url.URL, error) {
	if len(e.service.Path) > 1 {
		path = strings.TrimPrefix(path, e.service.Path)
	}
	raw := fmt.Sprintf("%s%s", e.service.Endpoint, path)
	return url.Parse(raw)
}

func (e *Default) proxy(ctx context.Context, address *url.URL) *httputil.ReverseProxy {
	p := httputil.NewSingleHostReverseProxy(address)

	transport := http.DefaultTransport.(*http.Transport)
	transport.ResponseHeaderTimeout = 10 * time.Second

	// Skip certificate verification if debugging
	if e.debug {
		transport.TLSClientConfig = &tls.Config{
			InsecureSkipVerify: true,
		}
	}

	p.Transport = newrelic.NewRoundTripper(transport)

	p.Director = func(request *http.Request) {
		request.Host = address.Host
		request.URL.Scheme = address.Scheme
		request.URL.Host = address.Host
		request.URL.Path = address.Path
	}

	p.ErrorHandler = func(w http.ResponseWriter, r *http.Request, err error) {
		zap.S().Errorf("error in proxy: %v", err)
		w.WriteHeader(http.StatusBadGateway)
	}

	p.ModifyResponse = func(response *http.Response) error {
		response.Header.Del("X-Powered-By")

		traceIDValue := ctx.Value(middleware.TraceIDContextKey)
		if traceIDValue == nil {
			return fmt.Errorf("trace ID missing in context")
		}

		traceID, ok := traceIDValue.(string)
		if !ok {
			return fmt.Errorf("trace ID is not string")
		}

		zap.S().Infow("service call",
			zap.String("traceID", traceID),
			zap.String("serviceAddress", address.String()),
			zap.Int("serviceStatusCode", response.StatusCode),
		)

		if response.StatusCode == http.StatusInternalServerError {
			defer func(Body io.ReadCloser) {
				_ = Body.Close()
			}(response.Body)

			response.Body = io.NopCloser(bytes.NewReader(nil))
			response.ContentLength = int64(0)
			response.Header.Set("Content-Length", strconv.Itoa(0))
		}

		return nil
	}

	return p
}
