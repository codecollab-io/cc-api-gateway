package middleware

import (
	chimiddleware "github.com/go-chi/chi/v5/middleware"
	"go.uber.org/zap"
	"net/http"
	"time"
)

func RequestLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ww := chimiddleware.NewWrapResponseWriter(w, r.ProtoMajor)

		traceIDValue := r.Context().Value(TraceIDContextKey)
		if traceIDValue == nil {
			zap.S().Error("trace ID missing in context")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		traceID, ok := traceIDValue.(string)
		if !ok {
			zap.S().Error("trace ID is not a string")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		start := time.Now()
		defer func() {
			end := time.Now()
			go func() {
				zap.S().Infow("http request",
					zap.String("traceID", traceID),
					zap.String("method", r.Method),
					zap.String("url", r.URL.String()),
					zap.Int("statusCode", ww.Status()),
					zap.Int("bytesWritten", ww.BytesWritten()),
					zap.Int("durationMilli", int(end.Sub(start).Milliseconds())),
				)
			}()
		}()

		next.ServeHTTP(ww, r)
	})
}
