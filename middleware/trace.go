package middleware

import (
	"context"
	"github.com/google/uuid"
	"net/http"
)

type contextKey string
var TraceIDContextKey contextKey = "traceID"

func Trace(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ID := uuid.New()

		ctx := context.WithValue(r.Context(), TraceIDContextKey, ID.String())
		r = r.WithContext(ctx)

		w.Header().Set("X-Powered-By", "CodeCollab API Gateway")
		w.Header().Set("X-Trace-ID", ID.String())

		next.ServeHTTP(w, r)
	})
}
