package middleware

import (
	gojwt "github.com/golang-jwt/jwt"
	"github.com/mssola/user_agent"
	"github.com/oschwald/geoip2-golang"
	"gitlab.com/codecollab-io/cc-api-gateway/config"
	"gitlab.com/codecollab-io/cc-api-gateway/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
	"strings"

	"bytes"
	"crypto/rsa"
	"encoding/json"
	"errors"
	"io"
	"net"
	"net/http"
	"strconv"
	"time"
)

type jwt struct {
	config     *config.Config
	collection *mongo.Collection
	publicKey  *rsa.PublicKey
	privateKey *rsa.PrivateKey
	db         *geoip2.Reader
}

type claims struct {
	*gojwt.StandardClaims
	UserID         string `json:"id"` // UserID represents the ID field in entity.User
	TokenID        string `json:"no"` // TokenID represents the ID field in entity.Token
	Email          string `json:"email"`
	ExpiresOn      int    `json:"expiresOn"` // ExpiresOn represents the ExpiresOn field in entity.Token
	Username       string `json:"uname"`
	DisplayName    string `json:"dname"`
	ProfilePicture string `json:"profilePic"`
}

type ltiClaims struct {
	*gojwt.StandardClaims
	InstitutionID string `json:"institutionID"`
	TeacherID     string `json:"teacherID"`
	ConsumerKey   string `json:"consumerKey"`
	Role          string `json:"role"`
}

func NewJWT(config *config.Config, collection *mongo.Collection, publicKey *rsa.PublicKey, privateKey *rsa.PrivateKey, db *geoip2.Reader) *jwt {
	return &jwt{config, collection, publicKey, privateKey, db}
}

func serveHTTP(
	handler http.Handler,
	w http.ResponseWriter,
	r *http.Request,
	body map[string]interface{},
	userInformation *entity.User,
	l *ltiClaims,
	token string,
) error {
	form := strings.Contains(r.Header.Get("Content-Type"), "application/x-www-form-urlencoded")
	if form {
		// Since LTI doesn't require this, and it's the only endpoint using form, we just serve without modification
		handler.ServeHTTP(w, r)
		return nil
	}

	if r.Method == "GET" || r.MultipartForm != nil {
		q := r.URL.Query()
		if userInformation == nil {
			q.Add("ui", "null")
		} else {
			u, err := json.Marshal(userInformation)
			if err != nil {
				return err
			}
			q.Add("ui", string(u))
		}
		if l == nil {
			q.Add("lti_ui", "null")
		} else {
			u, err := json.Marshal(l)
			if err != nil {
				return err
			}
			q.Add("lti_ui", string(u))
		}
		q.Add("isAuthenticated", strconv.FormatBool(userInformation != nil || l != nil))
		q.Add("token", token)
		r.URL.RawQuery = q.Encode()

		handler.ServeHTTP(w, r)
		return nil
	}

	body["auth"] = struct {
		UserInformation    *entity.User `json:"ui"`
		LTIUserInformation *ltiClaims   `json:"lti_ui"`
		IsAuthenticated    bool         `json:"isAuthenticated"`
		Token              string       `json:"token"`
	}{
		UserInformation:    userInformation,
		LTIUserInformation: l,
		IsAuthenticated:    userInformation != nil || l != nil,
		Token:              token,
	}

	marshal, err := json.Marshal(body)
	if err != nil {
		return err
	}

	r.Body = io.NopCloser(bytes.NewReader(marshal))
	// Set to prevent socket hangup
	r.ContentLength = int64(len(marshal))
	r.Header.Set("Content-Length", strconv.Itoa(len(marshal)))

	handler.ServeHTTP(w, r)
	return nil
}

// NewHandler creates a new handler, required specifies whether a Bearer token is required
// and validateCountry specifies whether a country must be determined
func (j *jwt) NewHandler(required bool, validateCountry bool) func(http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			t := r.Context().Value(TraceIDContextKey).(string)
			traceID := zap.String("traceID", t)

			defer func(Body io.ReadCloser) {
				_ = Body.Close()
			}(r.Body)

			// If request is JSON, this will be empty
			body := make(map[string]interface{})
			isMultipartForm := strings.Contains(r.Header.Get("Content-Type"), "multipart/form-data")
			isForm := strings.Contains(r.Header.Get("Content-Type"), "application/x-www-form-urlencoded")

			var bodyBytes []byte
			hasBody := r.Body != nil
			if hasBody {
				bodyBytes, _ = io.ReadAll(r.Body)
				r.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))
			}

			// If request is form, try and parse
			// Else, set headers before I forget
			if isMultipartForm {
				r.Body = http.MaxBytesReader(w, r.Body, 1024*1024*5) // Limit size to 5MB
				if err := r.ParseMultipartForm(1024 * 1024 * 5); err != nil {
					zap.S().Infow("error while parsing multipart form", zap.Error(err))
					http.Error(w, http.StatusText(http.StatusRequestEntityTooLarge), http.StatusRequestEntityTooLarge)
					return
				}
				if hasBody {
					r.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))
				}
			} else if isForm {
				err := r.ParseForm()
				if err != nil {
					zap.S().Infow("error while parsing form", zap.Error(err))
					http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				}
				if hasBody {
					r.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))
				}
			} else {
				if len(bodyBytes) > 0 {
					err := json.Unmarshal(bodyBytes, &body)
					if err != nil {
						zap.S().Errorw("error while unmarshalling body",
							zap.Error(err),
							traceID,
						)
						http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
						return
					}
				}
				r.Header.Set("Content-Type", "application/json")
			}

			var ltiTokenClaims *ltiClaims
			// If an LTI token was specified, it takes priority over the normal token
			ltiToken, err := r.Cookie("lti_token")
			if err == nil {
				token, err := gojwt.ParseWithClaims(ltiToken.Value, &ltiClaims{}, func(token *gojwt.Token) (interface{}, error) {
					return j.publicKey, nil
				})
				if err != nil {
					cast, ok := err.(*gojwt.ValidationError)
					if !ok {
						zap.S().Errorw("error while parsing JWT",
							zap.Error(err),
							traceID,
						)
						http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
						return
					}

					if cast.Errors != gojwt.ValidationErrorExpired {
						zap.S().Errorw("error while validating JWT",
							zap.String("error", cast.Error()),
							traceID,
						)
						http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
						return
					}
				}

				var ok bool
				ltiTokenClaims, ok = token.Claims.(*ltiClaims)
				if !ok {
					zap.S().Warnw("token claims are incorrect",
						zap.Error(err),
					)
					http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
					return
				}
			}

			cookie, err := r.Cookie("token")
			// Token not provided
			if err != nil {
				if required {
					zap.S().Infow("cookie required but was not provided", traceID)
					http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
					return
				}
				if err = serveHTTP(handler, w, r, body, nil, ltiTokenClaims, ""); err != nil {
					zap.S().Errorw(
						"error while serving response",
						zap.Error(err),
						traceID,
					)
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				return
			}

			// Token provided, parse token
			token, err := gojwt.ParseWithClaims(cookie.Value, &claims{}, func(token *gojwt.Token) (interface{}, error) {
				return j.publicKey, nil
			})
			if err != nil {
				cast, ok := err.(*gojwt.ValidationError)
				if !ok {
					zap.S().Errorw("error while parsing JWT",
						zap.Error(err),
						traceID,
					)
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
					return
				}

				if cast.Errors != gojwt.ValidationErrorExpired {
					zap.S().Errorw("error while validating JWT",
						zap.String("error", cast.Error()),
						traceID,
					)
					http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
					return
				}
			}

			tokenClaims, ok := token.Claims.(*claims)
			if !ok {
				zap.S().Warnw("token claims are incorrect",
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}

			userID, err := primitive.ObjectIDFromHex(tokenClaims.UserID)
			if err != nil {
				zap.S().Errorw("user ID from token claims is incorrect",
					zap.String("userID", userID.Hex()),
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			// Find user
			filter := bson.M{"_id": userID}
			result := j.collection.FindOne(r.Context(), filter)

			if errors.Is(result.Err(), mongo.ErrNoDocuments) {
				zap.S().Warnw("user not found",
					zap.String("userID", userID.Hex()),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}

			if result.Err() != nil {
				zap.S().Errorw("error trying to find user",
					zap.String("userID", tokenClaims.UserID),
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			user := new(entity.User)
			err = result.Decode(user)
			if err != nil {
				zap.S().Errorw("error trying to decode user",
					zap.String("userID", tokenClaims.UserID),
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			// If token is valid
			if tokenClaims.VerifyExpiresAt(time.Now().Unix(), true) {
				if err = serveHTTP(handler, w, r, body, user, ltiTokenClaims, cookie.Value); err != nil {
					zap.S().Errorw("error while serving response",
						zap.Error(err),
						traceID,
					)
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				return
			}

			// Find session
			sessionIndex := -1
			for idx, token := range user.Tokens {
				if token.ID == tokenClaims.TokenID {
					sessionIndex = idx
				}
			}

			// Session does not exist
			if sessionIndex == -1 {
				if err = serveHTTP(handler, w, r, body, nil, ltiTokenClaims, cookie.Value); err != nil {
					zap.S().Errorw("error while serving response",
						zap.Error(err),
						traceID,
					)
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				return
			}

			// Session expired
			session := user.Tokens[sessionIndex]
			if int64(session.ExpiresOn) <= time.Now().Unix() {
				if err = serveHTTP(handler, w, r, body, nil, ltiTokenClaims, cookie.Value); err != nil {
					zap.S().Errorw("error while serving response",
						zap.Error(err),
						traceID,
					)
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				return
			}

			remoteAddr := r.Header.Get("X-Forwarded-For")
			if len(remoteAddr) == 0 {
				remoteAddr = r.Header.Get("X-Real-Ip")
			}

			if len(remoteAddr) == 0 && validateCountry {
				zap.S().Warnw("no remote address available",
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}

			IP := net.ParseIP(remoteAddr)
			if IP == nil && validateCountry {
				zap.S().Warnw("could not determine IP from remote address",
					zap.String("remoteAddr", remoteAddr),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				return
			}

			var country *geoip2.Country
			if validateCountry {
				country, err = j.db.Country(IP)
				if err != nil {
					zap.S().Errorw("error during IP lookup",
						zap.Error(err),
						traceID,
					)
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
					return
				}
			} else {
				country = &geoip2.Country{
					Country: struct {
						GeoNameID         uint              `maxminddb:"geoname_id"`
						IsInEuropeanUnion bool              `maxminddb:"is_in_european_union"`
						IsoCode           string            `maxminddb:"iso_code"`
						Names             map[string]string `maxminddb:"names"`
					}{
						IsoCode: "SG",
						Names:   map[string]string{"en": "Singapore"},
					},
				}
			}

			// Generate new token
			newToken := gojwt.New(gojwt.GetSigningMethod("RS256"))
			newToken.Claims = &claims{
				StandardClaims: &gojwt.StandardClaims{
					ExpiresAt: time.Now().Add(1 * time.Hour).Unix(),
					IssuedAt:  time.Now().Unix(),
					Issuer:    "CodeCollab API Gateway",
					NotBefore: time.Now().Unix(),
					Subject:   user.ID.Hex(),
				},
				UserID:         user.ID.Hex(),
				TokenID:        session.ID,
				Email:          user.Email,
				ExpiresOn:      session.ExpiresOn,
				Username:       user.Username,
				DisplayName:    user.DisplayName,
				ProfilePicture: user.ProfilePicture,
			}

			newTokenString, err := newToken.SignedString(j.privateKey)
			if err != nil {
				zap.S().Errorw("error trying to sign new token",
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			// Find UA
			ua := user_agent.New(r.UserAgent())
			browserName, _ := ua.Browser()

			filter = bson.M{"_id": user.ID, "tokens.id": session.ID}
			_, err = j.collection.UpdateOne(r.Context(), filter, bson.M{
				"$set": bson.M{
					"tokens.$": entity.Token{
						ID:        session.ID,
						ExpiresOn: session.ExpiresOn,
						LastUsed: entity.LastUsed{
							Date: time.Now().UnixMilli(),
							IP:   IP.String(),
							Country: entity.Country{
								Code: country.Country.IsoCode,
								Name: country.Country.Names["en"],
							},
						},
						UA: entity.UA{
							Browser: browserName,
							OS:      strings.Replace(ua.OSInfo().Name, "_", ".", -1),
						},
					},
				},
			})

			if err != nil {
				zap.S().Errorw("error trying to update user",
					zap.String("userID", user.ID.Hex()),
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			projection := bson.D{{ Key: "tokens", Value: 0 }}
			result = j.collection.FindOne(r.Context(), bson.M{"_id": user.ID}, options.FindOne().SetProjection(projection))

			if result.Err() != nil {
				zap.S().Errorw("error trying to find user after updating",
					zap.String("userID", user.ID.Hex()),
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			err = result.Decode(user)
			if err != nil {
				zap.S().Errorw("error trying to decode user with ID",
					zap.String("userID", user.ID.Hex()),
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				return
			}

			if len(user.Tokens) > 5 {
				user.Tokens = user.Tokens[:0]
			}

			user.Tokens = user.Tokens[:0]

			var domain string
			if !j.config.Debug {
				domain = j.config.CookieDomain
			}
			http.SetCookie(w, &http.Cookie{
				Name:     "token",
				Value:    newTokenString,
				Path:     "/",
				Domain:   domain,
				Expires:  time.Now().Add(720 * time.Hour),
				MaxAge:   int(720 * time.Hour.Seconds()),
				Secure:   !j.config.Debug,
				HttpOnly: true,
				SameSite: http.SameSiteNoneMode,
			})

			if err = serveHTTP(handler, w, r, body, user, ltiTokenClaims, newTokenString); err != nil {
				zap.S().Errorw("error while serving response",
					zap.Error(err),
					traceID,
				)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		})
	}
}
