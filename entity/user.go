package entity

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	ID             primitive.ObjectID `bson:"_id" json:"_id"`
	Email          string             `bson:"email" json:"email"`
	Username       string             `bson:"uname" json:"uname"`
	DisplayName    string             `bson:"dname" json:"dname"`
	ProfilePicture string             `bson:"profilePic" json:"profilePic"`
	Tokens         []Token            `bson:"tokens" json:"tokens"`
}

// Token is actually a session the user has
type Token struct {
	ID        string   `bson:"id" json:"id"`
	ExpiresOn int      `bson:"expiresOn" json:"expiresOn"`
	LastUsed  LastUsed `bson:"lastUsed" json:"lastUsed"`
	UA        UA       `bson:"data" json:"data"`
}

type LastUsed struct {
	Date    int64   `bson:"date" json:"date"`
	IP      string  `bson:"ip" json:"ip"`
	Country Country `bson:"data" json:"data"`
}

type Country struct {
	Code string `bson:"code" json:"code"`
	Name string `bson:"name" json:"name"`
}

type UA struct {
	Browser string `bson:"browser" json:"browser"`
	OS      string `bson:"os" json:"os"`
}
