package main

import (
	"github.com/go-chi/chi"
	chimiddleware "github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/httprate"
	"github.com/newrelic/go-agent/v3/integrations/nrmongo"
	"github.com/newrelic/go-agent/v3/integrations/nrzap"
	"github.com/newrelic/go-agent/v3/newrelic"
	"github.com/oschwald/geoip2-golang"
	"gitlab.com/codecollab-io/cc-api-gateway/config"
	"gitlab.com/codecollab-io/cc-api-gateway/middleware"
	"gitlab.com/codecollab-io/cc-api-gateway/proxy"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"

	"context"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	serverConfig, err := config.NewConfig()
	if err != nil {
		panic(err)
	}

	var logger *zap.Logger
	if serverConfig.Debug {
		logger, err = zap.NewDevelopment()
	} else {
		logger, err = zap.NewProduction()
	}
	zap.ReplaceGlobals(logger)

	zap.S().Info("starting cc-api-gateway")

	zap.S().Info("creating newrelic application...")

	app := new(newrelic.Application)
	if !serverConfig.Debug {
		app, err = newrelic.NewApplication(
			newrelic.ConfigAppName(serverConfig.NewRelic.AppName),
			newrelic.ConfigLicense(serverConfig.NewRelic.License),
			nrzap.ConfigLogger(zap.L()),
		)
		if err != nil {
			zap.S().Fatal(err.Error())
		}
	}

	zap.S().Info("connecting to mongo...")

	nrMon := nrmongo.NewCommandMonitor(nil)
	clientOptions := options.Client().ApplyURI(serverConfig.Mongo.ConnectionString).SetMonitor(nrMon)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		zap.S().Fatal(err.Error())
	}

	zap.S().Info("preparing jwt keys...")

	publicRSA, _ := pem.Decode([]byte(serverConfig.PublicKey))
	if publicRSA == nil || publicRSA.Bytes == nil {
		zap.S().Fatal("invalid public key")
	}

	publicPKIX, err := x509.ParsePKIXPublicKey(publicRSA.Bytes)
	if err != nil {
		zap.S().Fatal(err.Error())
	}

	publicKey := publicPKIX.(*rsa.PublicKey)

	privateRSA, _ := pem.Decode([]byte(serverConfig.PrivateKey))
	if privateRSA == nil || privateRSA.Bytes == nil {
		zap.S().Fatal("invalid private key")
	}

	privateKey, err := x509.ParsePKCS1PrivateKey(privateRSA.Bytes)
	if err != nil {
		zap.S().Fatal(err.Error())
	}

	zap.S().Info("reading geoip2 database...")

	db, err := geoip2.Open("GeoLite2-Country.mmdb")
	if err != nil {
		zap.S().Fatal(err.Error())
	}

	zap.S().Info("creating services...")

	jwtMiddleware := middleware.NewJWT(serverConfig, client.Database(serverConfig.Mongo.Database).Collection(serverConfig.Mongo.UserCollection), publicKey, privateKey, db)

	mux := chi.NewRouter()
	_, notFoundHandler := newrelic.WrapHandleFunc(app, "/", mux.NotFoundHandler())
	_, methodNotAllowedHandler := newrelic.WrapHandleFunc(app, "/", mux.MethodNotAllowedHandler())
	mux.NotFound(notFoundHandler)
	mux.MethodNotAllowed(methodNotAllowedHandler)

	mux.Use(chimiddleware.Recoverer)

	// middleware.Trace must be the first middleware as others may depend on the TraceID set in request context
	mux.Use(middleware.Trace)
	mux.Use(middleware.RequestLogger)

	mux.Use(httprate.Limit(
		60,
		1*time.Minute,
		httprate.WithKeyFuncs(httprate.KeyByIP, httprate.KeyByEndpoint),
		httprate.WithLimitHandler(func(w http.ResponseWriter, r *http.Request) {
			http.Error(w, http.StatusText(http.StatusTooManyRequests), http.StatusTooManyRequests)
		}),
	))

	mux.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*.cclb.dev", "https://*.codecollab.io", "https://codecollab.io", "https://localhost:3000", "http://localhost:3000", "https://*.vercel.app"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))

	// For JWT middleware, if Debug is true, do not validate country

	// cc-auth
	authMux := chi.NewRouter()
	authProxy := proxy.NewDefault(serverConfig.Debug, serverConfig.ServiceMap.Auth)
	// cc-auth unauthenticated
	authMux.Get("/*", authProxy.Handler)
	authMux.Post("/*", authProxy.Handler)
	// cc-auth authenticated
	authMux.Group(func(r chi.Router) {
		r.Use(jwtMiddleware.NewHandler(false, !serverConfig.Debug))
		r.Post("/updateInfo", authProxy.Handler)
		r.Post("/logout", authProxy.Handler)
		r.Post("/deleteAccount", authProxy.Handler)
	})
	mux.Mount(newrelic.WrapHandle(app, serverConfig.ServiceMap.Auth.Path, authMux))

	// cc-api
	apiMux := chi.NewRouter()
	apiProxy := proxy.NewDefault(serverConfig.Debug, serverConfig.ServiceMap.API)
	// cc-api unauthenticated
	apiMux.Get("/*", apiProxy.Handler)
	apiMux.Post("/*", apiProxy.Handler)
	// cc-api authenticated
	apiMux.Group(func(r chi.Router) {
		r.Use(jwtMiddleware.NewHandler(false, !serverConfig.Debug))
		r.Get("/projs*", apiProxy.Handler)
		r.Post("/projs*", apiProxy.Handler)
		r.Get("/ws*", apiProxy.Handler)
		r.Post("/ws*", apiProxy.Handler)
	})
	mux.Mount(newrelic.WrapHandle(app, serverConfig.ServiceMap.API.Path, apiMux))

	// cc-editor-api
	editorMux := chi.NewRouter()
	editorProxy := proxy.NewDefault(serverConfig.Debug, serverConfig.ServiceMap.Editor)
	// cc-editor-api authenticated
	editorMux.Group(func(r chi.Router) {
		r.Use(jwtMiddleware.NewHandler(false, !serverConfig.Debug))
		r.Get("/*", editorProxy.Handler)
		r.Post("/*", editorProxy.Handler)
	})
	mux.Mount(newrelic.WrapHandle(app, serverConfig.ServiceMap.Editor.Path, editorMux))

	// cc-lti
	ltiMux := chi.NewRouter()
	ltiProxy := proxy.NewDefault(serverConfig.Debug, serverConfig.ServiceMap.LTI)
	// cc-lti authenticated
	ltiMux.Group(func(r chi.Router) {
		r.Use(jwtMiddleware.NewHandler(false, !serverConfig.Debug))
		r.Get("/*", ltiProxy.Handler)
		r.Put("/*", ltiProxy.Handler)
		r.Post("/*", ltiProxy.Handler)
		r.Delete("/*", ltiProxy.Handler)
	})
	mux.Mount(newrelic.WrapHandle(app, serverConfig.ServiceMap.LTI.Path, ltiMux))

	server := http.Server{Addr: serverConfig.ApplicationURL, Handler: mux}

	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, syscall.SIGTERM, syscall.SIGINT)

	zap.S().Infof("starting server on %s with debug %t", serverConfig.ApplicationURL, serverConfig.Debug)

	go func() {
		err = server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			zap.S().Fatalf("fatal error starting server: %v", err)
		}
	}()

	<-interruptChan
	ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = server.Shutdown(ctx)
	if err != nil {
		zap.S().Fatal("fatal error while shutting down: %v", err)
	}
}
