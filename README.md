## CodeCollab API Gateway

This is a high performance API Gateway connecting the internet to CodeCollab's services.

The gateway maps services as follows:

`https://cc-api-gateway.cclb.dev/:service-name/*`

will translate to

`https://cc-service-name.cclb.dev/*`

Service mappings are defined under `ServiceMap` in the config file.

### Notes

When `Debug = true` is set in the configuration, the gateway will skip TLS verification.